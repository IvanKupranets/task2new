using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace Epam_Task2
{
    class Program
    {
        class mnogochlen
        {
            public int poradok;
            public Hashtable table = new Hashtable();
            public Dictionary<double, double> dictionary = new Dictionary<double, double>();
            public mnogochlen(int poradok)
            {
                this.poradok = poradok;
            }
            public void SetPolinomialWithValues()
            {
                Random a = new Random();
                for (int i = poradok; i >= 0; i--)
                {
                    table.Add(i, Convert.ToInt32(a.NextDouble() * 10));
                }
            }
            public void ShowPolinomial()
            {
                foreach (DictionaryEntry i in table)
                {
                        Console.Write("{0}X^{1} ", i.Value, i.Key);
                }
                Console.WriteLine();
            }
            public void ShowPolinomialAfterMultiply()
            {
                foreach (var i in dictionary)
                {
                    Console.Write("{0}X^{1} ", i.Value, i.Key);
                }
                Console.WriteLine();
            }
            public double CountWithX(double x)
            {
                double result = 0;
                foreach (DictionaryEntry i in table)
                {

                    result += Math.Pow(x, Convert.ToDouble(i.Key)) * Convert.ToDouble(i.Value);

                }
                return result;
            }
            public static mnogochlen SumPolinomial(mnogochlen mn1, mnogochlen mn2)
            {
                mnogochlen res = new mnogochlen(mn1.table.Count - 1);
                int index;
                if (mn1.table.Count == mn2.table.Count)
                {
                    index = mn1.table.Count - 1;
                    for (int i = mn1.table.Count - 1; i >= 0; i--)
                    {
                        res.table.Add(index, Convert.ToDouble(mn1.table[i]) + Convert.ToDouble(mn2.table[i]));
                        index--;
                    }
                    return res;
                }
                if (mn1.table.Count != mn2.table.Count)
                {
                    int riznPoriad;
                    if (mn1.poradok > mn2.poradok)
                    {
                        res = new mnogochlen(mn1.table.Count - 1);
                        riznPoriad = mn1.poradok - mn2.poradok;
                        index = mn1.table.Count - 1;
                        for (int i = mn1.table.Count - 1; i >= mn1.table.Count - mn1.poradok; i--)
                        {
                            res.table.Add(index, mn1.table[i]);
                            index--;
                            if (index == (mn1.table.Count - 1 - riznPoriad))
                            {
                                for (int j = mn1.table.Count - 1 - riznPoriad; j >= 0; j--)
                                {
                                    res.table.Add(index, Convert.ToDouble(mn1.table[j]) + Convert.ToDouble(mn2.table[j]));
                                    index--;
                                }
                                return res;
                            }
                        }
                    }

                    if (mn1.poradok < mn2.poradok)
                    {
                        riznPoriad = mn2.poradok - mn1.poradok;
                        res = new mnogochlen(mn1.table.Count - 1);
                        index = mn2.table.Count - 1;
                        for (int i = mn2.table.Count - 1; i >= mn2.table.Count - mn2.poradok; i--)
                        {
                            res.table.Add(index, mn2.table[i]);
                            index--;
                            if (index == (mn2.table.Count - 1 - riznPoriad))
                            {
                                for (int j = mn2.table.Count - 1 - riznPoriad; j >= 0; j--)
                                {
                                    res.table.Add(index, Convert.ToDouble(mn2.table[j]) + Convert.ToDouble(mn1.table[j]));
                                    index--;
                                }
                                return res;
                            }
                        }
                    }
                }
                return res;
            }
            public static mnogochlen DifferencePolinomial(mnogochlen mn1, mnogochlen mn2)
            {
                mnogochlen res = new mnogochlen(mn1.table.Count - 1);
                int index;
                if (mn1.table.Count == mn2.table.Count)
                {
                    index = mn1.table.Count - 1;
                    for (int i = mn1.table.Count - 1; i >= 0; i--)
                    {
                        res.table.Add(index, Convert.ToDouble(mn1.table[i]) - Convert.ToDouble(mn2.table[i]));
                        index--;
                    }
                    return res;
                }
                if (mn1.table.Count != mn2.table.Count)
                {
                    int riznPoriad;
                    if (mn1.poradok > mn2.poradok)
                    {
                        res = new mnogochlen(mn1.table.Count - 1);
                        riznPoriad = mn1.poradok - mn2.poradok;
                        index = mn1.table.Count - 1;
                        for (int i = mn1.table.Count - 1; i >= mn1.table.Count - mn1.poradok; i--)
                        {
                            res.table.Add(index, mn1.table[i]);
                            index--;
                            if (index == (mn1.table.Count - 1 - riznPoriad))
                            {
                                for (int j = mn1.table.Count - 1 - riznPoriad; j >= 0; j--)
                                {
                                    res.table.Add(index, Convert.ToDouble(mn1.table[j]) - Convert.ToDouble(mn2.table[j]));
                                    index--;
                                }
                                return res;
                            }
                        }
                    }

                    if (mn1.poradok < mn2.poradok)
                    {
                        riznPoriad = mn2.poradok - mn1.poradok;
                        res = new mnogochlen(mn1.table.Count - 1);
                        index = mn2.table.Count - 1;
                        for (int i = mn2.table.Count - 1; i >= mn2.table.Count - mn2.poradok; i--)
                        {
                            res.table.Add(index, mn2.table[i]);
                            index--;
                            if (index == (mn2.table.Count - 1 - riznPoriad))
                            {
                                for (int j = mn2.table.Count - 1 - riznPoriad; j >= 0; j--)
                                {
                                    res.table.Add(index, Convert.ToDouble(mn2.table[j]) - Convert.ToDouble(mn1.table[j]));
                                    index--;
                                }
                                return res;
                            }
                        }
                    }
                }
                return res;
            }
            public static mnogochlen MultiplyPolinomial(mnogochlen mn1, mnogochlen mn2)
            {
                mnogochlen res = new mnogochlen((mn1.table.Count * mn2.table.Count) -1);
                 
                for (int i = mn1.table.Count - 1; i >= 0; i--)
                {
                    for (int j = mn2.table.Count - 1; j >= 0; j--)
                    {
                        if (res.dictionary.Keys.Contains(i + j))
                        {
                            res.dictionary[i + j] += Convert.ToDouble(mn1.table[i]) * Convert.ToDouble(mn2.table[j]);
                        }
                        else
                        {  //res.table.Add(i + j, Convert.ToDouble(mn1.table[i]) * Convert.ToDouble(mn2.table[j]));
                           res.dictionary.Add(i + j, Convert.ToDouble(mn1.table[i]) * Convert.ToDouble(mn2.table[j]));
                        }   
                    }
                }
                return res;
            }
                
            static void Main(string[] args)
            {
                mnogochlen mn1 = new mnogochlen(2);
                mn1.SetPolinomialWithValues();
                mnogochlen mn2 = new mnogochlen(2);
                mn2.SetPolinomialWithValues();
                mn1.ShowPolinomial();
                Console.WriteLine("znachenia mn1 for x = 1 : mn1 = {0}", mn1.CountWithX(1));
                mn2.ShowPolinomial();
                Console.WriteLine("znachenia mn1 for x = 1 : mn1 = {0}", mn2.CountWithX(1));
                mnogochlen mn3 = mnogochlen.SumPolinomial(mn1, mn2);
                Console.WriteLine("Suma mnogochlen mn1 and mn2 ");
                mn3.ShowPolinomial();
                mnogochlen mn4 = mnogochlen.DifferencePolinomial(mn1, mn2);
                Console.WriteLine("Riznucia mnogochlen mn1 and mn2 ");
                mn4.ShowPolinomial();
                mnogochlen mn5 = mnogochlen.MultiplyPolinomial(mn1, mn2);
                Console.WriteLine("Multiply mnogochlen mn1 and mn2 ");
                mn5.ShowPolinomialAfterMultiply();
                Console.ReadLine();
            }
        }

    }
}